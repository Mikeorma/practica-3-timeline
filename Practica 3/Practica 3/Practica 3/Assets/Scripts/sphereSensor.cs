﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sphereSensor : MonoBehaviour {

	public bool Touch { get; set; }
    public string tag;

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag(tag))
        {
            Touch = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(tag))
        {
            Touch = false;
        }

    }
    private void Update()
    {
        Debug.Log(name +" " + Touch);
        
    }



}
