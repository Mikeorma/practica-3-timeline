﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]


public class CharacterAnimBasedMovement : MonoBehaviour {

    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;

    [Header("Animator Parameters")]
    public string motionParam = "Motion";
    private bool mirrorIdle;
    public string mirrorIdleParam = "mirror_IdleParam";
    public string turn180Param = "Turn180";
    private bool turn180;
    public float degreesToTurn = 135f;
    public string stairsParam = "Stairs_Param";
    private bool stairs;
    public bool canMove = true;
    private bool grounded = false;
    public string isGroundedParam = "isGrounded";


    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float startAnimTime = 0.3f;
    [Range(0, 1f)]
    public float stopAnimTime = 0.15f;

    public float speed;
    public Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;

    PlayerTPSController TPSController;


    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        TPSController = GetComponent<PlayerTPSController>();
        
    }


    public void MoveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash, bool stairsUp)
    {

        

        //Calculate input
        speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        //dash if max speed is reached
        if (speed >= speed - rotationThreshold && dash)
        {
            speed = 1.5f;
        }

        //physically move
        if (speed > rotationThreshold && characterController.isGrounded && stairsUp == false)
        {
            grounded = true;
            animator.SetBool(isGroundedParam, grounded);

            stairs = false;
            animator.SetBool(stairsParam, stairs);
            animator.SetFloat(motionParam, speed, startAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);


            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                turn180 = true;

            }
            else
            {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
            }

            //180 turn
            animator.SetBool(turn180Param, turn180);
            //move
            animator.SetFloat(motionParam, speed, startAnimTime, Time.deltaTime);
            if (jump && characterController.isGrounded)
            {
                animator.SetTrigger("Jump_T");

            }



        } else if (speed > rotationThreshold && characterController.isGrounded && stairsUp == true){

            grounded = true;
            animator.SetBool(isGroundedParam, grounded);
            stairs = true;
            animator.SetBool(stairsParam, stairs);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);

        }
        else if (speed < rotationThreshold)
        {
            grounded = true;
            animator.SetBool(isGroundedParam, grounded);

            stairs = false;
            animator.SetBool(stairsParam, stairs);
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, 0f, stopAnimTime, Time.deltaTime);
            if (jump && characterController.isGrounded)
            {
                animator.SetTrigger("Jump_T");

            }


/*
            if (Input.GetKeyDown(KeyCode.Space) && characterController.isGrounded)
            {
                jump = true;
                animator.SetTrigger("Jump_T");
                
            }
            */
        }


        //falling animation

        if (!characterController.isGrounded)
        {

            //characterController.Move(new Vector3(0f, 10.1f * Time.deltaTime , 0f));
            grounded = false;
            animator.SetBool(isGroundedParam, grounded);

        }

        /*
        //Jump
        if (Input.GetKeyDown(KeyCode.Space)&&characterController.isGrounded)
        {
            jump = true;
            animator.SetTrigger("Jump_T");
            characterController.Move(new Vector3(characterController.velocity.x, -9.8f * Time.deltaTime, characterController.velocity.z));
        }
        else
        {
            jump = false;
        }
        */
        /*
        if (stairs == true)
        {
            animator.SetBool(stairsParam, stairs);
            animator.SetFloat(motionParam, speed, startAnimTime, Time.deltaTime);

        }
        */

    }


    private void OnAnimatorIK(int layerIndex)
    {
        if (speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        //right in front
        if (distanceToRightFoot > distanceToLeftFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }

    }





}
